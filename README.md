
Usage example:
```bash
	Rscript de-analysis.R -q ../quantification.tsv -m conditions -o there
```


- de-dataset.tsv file is required, you can add as many columns as you want, you can use any names and you must reuse the same names in the model (-m) argument

- By default, the variables in the model are considered as factors (categorical variables). You must specify -c if it is continuous. You must edit the code (line 129-130 if you have both)

- After running de-analysis.R, run de-volcano.R to produce your plots

```bash
	Rscript de-volcano.R there "testing condition 1 against condition 2"
```

Enjoy 